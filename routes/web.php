<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Activation user.
Route::get('activation/{id}/{token}', 'Auth\ActivateController@activation')->name('activation');

Auth::routes();

Route::group(['middleware' => ['is.active.email']], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/chat', 'ChatController@index')->name('chat');
    Route::post('/chat/messages', 'ChatController@messages')->name('messages');
});

