<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use App\Http\Controllers\Controller;

class ActivateController extends Controller
{
    /**
     * Make user activation.
     */
    public function activation($userId, $token)
    {
        $user = User::findOrFail($userId);
        
        // Check token from url.
        if (md5($user->email) == $token) {
            // Change status and login user.
            $user->status = 1;
            
            \Log::info('$user');
            \Log::info($user);
            
            $user->save();

            \Session::flash('flash_message', trans('interface.ActivatedSuccess'));

            // Make login user.
            Auth::login($user, true);
        } else {
            // Wrong token.
            \Session::flash('flash_message_error', trans('interface.ActivatedWrong'));
        }
        return redirect('/');
    }
}
