<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'vadim', 'email' => 'vadim@gmail.com', 'status' => 1, 'password' => bcrypt('password')],
            ['name' => 'sanya', 'email' => 'sanya@gmail.com', 'status' => 1, 'password' => bcrypt('password')]
        ];
        
        foreach ($items as $item) {
            User::create($item);
        }
    }
}
